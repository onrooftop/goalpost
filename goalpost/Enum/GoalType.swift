//
//  GoalType.swift
//  goalpost
//
//  Created by Panupong Kukutapan on 11/19/2560 BE.
//  Copyright © 2560 Panupong Kukutapan. All rights reserved.
//

import Foundation


enum GoalType: String {
    case longTerm = "Long Term"
    case shortTerm = "Short Term"
}

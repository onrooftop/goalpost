//
//  UIButtonExt.swift
//  goalpost
//
//  Created by Panupong Kukutapan on 11/20/2560 BE.
//  Copyright © 2560 Panupong Kukutapan. All rights reserved.
//

import UIKit

extension UIButton {
    func setSelectedColor() {
        self.backgroundColor = #colorLiteral(red: 0.4922404289, green: 0.7722371817, blue: 0.4631441236, alpha: 1)
    }
    
    func deSelectedColor() {
        self.backgroundColor = #colorLiteral(red: 0.7462929487, green: 0.888810575, blue: 0.7402062416, alpha: 1)
    }
}
